package com.example.convertchar;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class TextConverterApp extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Text Converter");

        // 创建一个 BorderPane 作为主布局容器
        BorderPane borderPane = new BorderPane();
        borderPane.setPrefSize(800, 600);

        // 创建一个垂直的 VBox 作为主要内容容器
        VBox mainVBox = new VBox(20);
        mainVBox.setPadding(new Insets(20));

        // 创建输入文本框，设置初始文本和换行
        TextArea inputTextArea = new TextArea();
        inputTextArea.setPromptText("Enter text to convert...");
        inputTextArea.setWrapText(true);
        inputTextArea.setPrefSize(400, 200); // 设置输入框的尺寸
        Insets inputTextAreaInsets = new Insets(0); // 设置输入框的上下左右间距

        // 创建输出文本框，设置换行
        TextArea outputTextArea = new TextArea();
        outputTextArea.setWrapText(true);
        outputTextArea.setEditable(false);
        outputTextArea.setPrefSize(400, 200); // 设置输出框的尺寸
        Insets outputTextAreaInsets = new Insets(0); // 设置输出框的上下左右间距

        // 创建 "Convert" 和 "Clear" 按钮
        Button convertButton = new Button("Convert");
        Button clearButton = new Button("Clear");
        convertButton.setPrefSize(100, 40); // 设置按钮的尺寸
        clearButton.setPrefSize(100, 40); // 设置按钮的尺寸
        Insets buttonInsets = new Insets(0, 10, 0, 10); // 设置按钮的左右间距

        // 创建一个横向的 HBox 放置按钮，并设置样式和拉伸
        HBox buttonBox = new HBox(20, convertButton, clearButton);
        buttonBox.setStyle("-fx-alignment: center;");
        HBox.setHgrow(convertButton, Priority.ALWAYS);
        HBox.setHgrow(clearButton, Priority.ALWAYS);

        // 设置 "Convert" 按钮点击事件
        convertButton.setOnAction(e -> {
            String inputText = inputTextArea.getText();
            String convertedText = convertText(inputText);
            outputTextArea.setText(convertedText);
            copyToClipboard(convertedText);
        });

        // 设置 "Clear" 按钮点击事件
        clearButton.setOnAction(e -> {
            inputTextArea.clear();
            outputTextArea.clear();
        });

        // 设置各个组件的间距和填充
        inputTextArea.setPadding(inputTextAreaInsets);
        outputTextArea.setPadding(outputTextAreaInsets);
        buttonBox.setPadding(new Insets(20));

        // 将输入框、按钮区域和输出框添加到 VBox
        mainVBox.getChildren().addAll(inputTextArea, buttonBox, outputTextArea);

        // 将 VBox 添加到 BorderPane 的中心
        borderPane.setCenter(mainVBox);

        // 创建场景并显示
        Scene scene = new Scene(borderPane);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    // 将文本转换为要求的格式
    private String convertText(String input) {
        String noChinese = input.replaceAll("[\\u4e00-\\u9fa5]", ""); // 删除中文字符
        String noPunctuation = noChinese.replaceAll("[^A-Za-z0-9\\s]", " "); // 删除非字母字符和非数字字符
        String upperCaseText = noPunctuation.toUpperCase(); // 转换为大写
        String singleSpace = upperCaseText.replaceAll("\\s+", " "); // 合并连续空格为一个
        return singleSpace.trim(); // 删除前后空格
    }

    // 复制文本到剪贴板
    private void copyToClipboard(String text) {
        Clipboard clipboard = Clipboard.getSystemClipboard();
        ClipboardContent content = new ClipboardContent();
        content.putString(text);
        clipboard.setContent(content);
    }
}

